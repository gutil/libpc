/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libpc
 * FILE NAME: PC_functions.h
 *
 * CONTRIBUTORS: Nicolas FLIPO
 *
 * LIBRARY BRIEF DESCRIPTION: Parallel computing management using open MP
 * (memory shared)
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libpc Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

/**
 * @file        PC_functions.h
 * @brief       PC library functions prototypes
 * @author      Nicolas FLIPO
 * @date        2024
 * @copyright   Eclipse Public License - v 2.0
 */

// in PC_anal_sys.c
int PC_numCPU(FILE *, int *);

// in PC_omp.c
int PC_set_active_proc(FILE *, int);
int PC_set_chunk_size(FILE *, int, int);
int PC_set_chunk_size_silent(FILE *, int, int);