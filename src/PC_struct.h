/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libpc
 * FILE NAME: PC_struct.h
 *
 * CONTRIBUTORS: Nicolas FLIPO
 *
 * LIBRARY BRIEF DESCRIPTION: Parallel computing management using open MP
 * (memory shared)
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libpc Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

/**
 * @file        PC_struct.h
 * @brief       PC library structures
 * @author      Nicolas FLIPO
 * @date        2024
 * @copyright   Eclipse Public License - v 2.0
 */

typedef struct share_mem_paral s_smp;

/**
 * @struct share_mem_paral
 * @brief Shared memory attributes for parallelized tasks
 */
struct share_mem_paral {
  /*!< Number of CPUs */
  double nthreads;
  /*!< Size of the chunks for a given operation */
  int chunk;
};

#define new_smp() ((s_smp *)malloc(sizeof(s_smp)))