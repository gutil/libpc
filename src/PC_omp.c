/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libpc
 * FILE NAME: PC_omp.c
 *
 * CONTRIBUTORS: Nicolas FLIPO
 *
 * LIBRARY BRIEF DESCRIPTION: Parallel computing management using open MP
 * (memory shared)
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libpc Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

/**
 * @file        PC_omp.c
 * @brief       Functions dealing with attributes ...
 * @author      Nicolas FLIPO
 * @date        2024
 * @copyright   Eclipse Public License - v 2.0
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <libprint.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include "PC_functions.h"
#include "PC_param.h"

/**
 * @brief Setting number of CPU of parallelization task according to max system specs and OMP_NUM_THREADS env variable value
 * @return If success, set number of CPU
 */
int PC_set_active_proc(FILE *fp, int npeff) {

  int tmp;
  int nproc;
  int nprocs_max;
  nprocs_max = -1;
  char *error;

  // NF 7/12/2017 : Checking if OMP_NUM_THREADS env variable is coherent with maximum number of CPUs of the system
  nprocs_max = PC_numCPU(fp, &nprocs_max);
  error = getenv("OMP_NUM_THREADS");

  if (error != NULL)
    tmp = atoi(error);
  else {
    LP_warning(fp, "libpc%4.2f %s in %s l%d: global variable OMP_NUM_THREADS not defined in the .bashrc or .bash_profile, set to 1\n", NVERSION_PC, __func__, __FILE__, __LINE__);
    tmp = 1;
  }

  LP_printf(fp, "libpc%4.2f : getting global var OMP_NUM_THREADS = %d\nMax procs = %d\n", NVERSION_PC, tmp, nprocs_max);

  if (tmp > nprocs_max) {
    LP_printf(fp, "libpc%4.2f : global variable OMP_NUM_THREADS larger than the max number of available processors. Forced to %d\n", NVERSION_PC, nprocs_max);
    tmp = nprocs_max;
  }

  // NF 7/12/2017
  if (npeff == PC_CODE) {
    nproc = nprocs_max;
    LP_printf(fp, "libpc%4.2f : Setting nproc to max available : %d\n", NVERSION_PC, nproc);
  } else {
    if (npeff == PC_NPROC_OMP) {
      nproc = tmp;
      LP_printf(fp, "libpc%4.2f : Setting nproc to OMP_NUM_THREADS : %d sur nproc %d\n", NVERSION_PC, nproc, nprocs_max);
    } else {
      if (npeff <= nprocs_max) {
        nproc = npeff;
        LP_printf(fp, "libpc%4.2f : Setting nproc to the required number : %d\n", NVERSION_PC, nproc);
      } else {
        nproc = nprocs_max;
        LP_warning(fp, "libpc%4.2f : Required number of processors out of range (%d) --> set to the maximum available : %d\n", NVERSION_PC, npeff, nproc);
      }
    }
  }

  LP_printf(fp, "libpc%4.2f : Using %d procs out of %d\n", NVERSION_PC, nproc, nprocs_max);

  return nproc;
}

/**
 * @brief Determines the chunk size (i.e. the number of cells) to be treated per CPU
 * @return If success, set chunk size per CPU
 */
int PC_set_chunk_size(FILE *fp, int nop, int nthreads) {
  int isize;
  isize = floor(nop / nthreads);

  if (isize < 1) {
    LP_warning(fp, "libpc%4.2f : Too much threads for the task, chunk size set to 1\n,", NVERSION_PC);
    isize = 1;
  }
  LP_printf(fp, "libpc%4.2f : Calculating Chunk size : nop %d / nproc %d = %d\n", NVERSION_PC, nop, nthreads, isize);

  return isize;
}

/**
 * @brief Determines the chunk size (i.e. the number of cells) to be treated per CPU
 * (same as PC_set_chunk_size() but with no warnings)
 * @return If success, set chunk size per CPU
 */
int PC_set_chunk_size_silent(FILE *fp, int nop, int nthreads) {
  int isize;
  isize = floor(nop / nthreads);

  if (isize < 1) {
    isize = 1;
  }
  return isize;
}