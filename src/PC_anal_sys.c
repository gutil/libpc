/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libpc
 * FILE NAME: PC_anal_sys.c
 *
 * CONTRIBUTORS: Nicolas FLIPO
 *
 * LIBRARY BRIEF DESCRIPTION: Parallel computing management using open MP
 * (memory shared)
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libpc Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

/**
 * @file        PC_anal_sys.c
 * @brief       Functions dealing with system calculations resources
 * @author      Nicolas FLIPO
 * @date        2024
 * @copyright   Eclipse Public License - v 2.0
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include "libprint.h"
#include "PC_functions.h"
#include "PC_param.h"

/**
 * @brief Determines of the number of CPU of the system
 * @return If successful, found number of CPU
 */
int PC_numCPU(FILE *fp, int *nprocs_max) {

  int nprocs = -1;
#ifdef _WIN32
#ifndef _SC_NPROCESSORS_ONLN
  SYSTEM_INFO info;
  GetSystemInfo(&info);
#define sysconf(a) info.dwNumberOfProcessors
#define _SC_NPROCESSORS_ONLN
#endif
#endif
#ifdef _SC_NPROCESSORS_ONLN
  nprocs = sysconf(_SC_NPROCESSORS_ONLN);
  if (nprocs < 1)
    LP_error(fp, "Could not determine number of CPUs online:\n%s\n", strerror(errno));
  *nprocs_max = sysconf(_SC_NPROCESSORS_CONF);
  if (*nprocs_max < 1)
    LP_error(fp, "Could not determine number of CPUs configured:\n%s\n", strerror(errno));

  LP_printf(fp, "%ld of %ld processors online\n", nprocs, *nprocs_max);
#else
  LP_error(fp, "Could not determine number of CPUs");
#endif

  return nprocs;
}